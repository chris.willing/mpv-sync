/*
  mpv-sync - mpvConnection.js
  Copyright (C) 2020  Christoph Willing  Brisbane, Australia

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


module.exports = class mpvConnection {
  #sync = true;
  #offset = 0.0;

  constructor (path, client) {
    this.path = path;
    this.client = client;
  }

  path() {
    return this.path;
  }

  set offset (val) {
    this.#offset = val;
  }
  get offset () {
    return this.#offset;
  }


  write (data) {
    this.client.write(data);
  }

};



/* ex:set ai shiftwidth=2 inputtab=spaces smarttab noautotab: */

