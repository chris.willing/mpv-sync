#!/usr/bin/env node

/*
  mpv-sync - reflect.js
  Copyright (C) 2020  Christoph Willing  Brisbane,Australia

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


const fs = require("fs");
const net = require('net');
const os = require('os');
const path = require('path');

const mpvConnection = require('./mpvConnection');



const maxClients = 256;
const socketPrefix = "mpvctl";
const pathBase = path.join(os.tmpdir(), socketPrefix);

var connections = {}; // {"path": mpvConnection}

var properties = {
  "speed" : 1.000000
};
var requests = {
  "req_show-text" : 16,
  "req_observe-speed" : 17,
  "req_observe-seek" : 18,
  "req_get-time-pos" : 19,
  "req_request_log_messages" : 20,
}


var timeOffset = 0.0;  // Seconds
var myArgs = process.argv.slice(2);
if ( myArgs.length == 0 ) {
  timeOffset = 0.0;
} else {
  timeOffset = myArgs[0];
}
console.log("Applying offset = " + timeOffset);


/*
  When a seek event is received from a particular path, it becomes the master
  via the initiate() function. All other paths become slaves. Further seek
  events from the slave paths are ignored until the slaves array is empty (and
  master is cleared). As events are detected on each slave path, that path
  is removed from the slaves array until the slaves array is empty, at which
  time the object is cleared - allowing the next seek event to have effect.
*/
var seekMaster = {
  master   : null,
  slaves   : [],
  timePos : null,

  /*
    initiate : function (path)
    Try to install path as master.
    Otherwise (if there's already a master) add path to slaves
  */
  initiate : function (path) {
    console.log("initiate(); enter from (" + path + ")");
    if ( this.master === null ) {
      this.master = path;
      console.log("initiate(); added master at (" + path + ")");
      return;
    }
    for (let i=0; i < maxClients; i++) {
      let target = pathBase + i;
      if ( fs.existsSync(target) ) {
        console.log("initiate(): target = " + target);
        if ( target == this.master) {
          console.log("initiate(): already have master at " + path + " (" + target + ")");
        } else {
          if ( this.slaves.includes(target) ) {
            console.log("initiate(): " + target + " is already in slaves");
            break;
          } else {
            this.slaves.push(target);
            console.log("initiate(): added " + path + " to slaves");
            break;
          }
        }
      }
    }
  },

  /*
    Remove path from the slaves array
  */
  update   : function (path) {
    for (let i=0; i < this.slaves.length; i++) {
      if ( this.slaves[i] === path ) {
        this.slaves.splice(i, 1);
        console.log("removed " + path + " from slaves");
      }
      if ( this.slaves.length <= 0 ) {
        this.clear();
      }
    }
  },

  /*
    Fill the timePos field
  */
  addTime  : function (time) {
    console.log("addTime(): " + time);
    this.timePos = time;
  },

  /*
    Clear the object - ready for new master
  */
  clear    : function () {
    this.timePos = null;
    this.master = null;
    this.slaves = [];
    console.log("cleared seekMaster");
  },

  isMaster  : function (path) {
    return path === this.master;
  },
  isSlave    : function (path) {
    return this.slaves.includes(path);
  },
  hasTime    : function () {
    return !(this.timePos == null);
  },

  dump       : function () {
    console.log("seekMaster master: " + this.master);
    console.log("seekMaster slaves: " + this.slaves);
  }
};


function makeNewConnections () {
  seekMaster.dump();
  console.log("Checking for new clients");
  for (let i=0; i < maxClients; i++) {
    let path = pathBase + i;
    //console.log("    - checking " + path);
    if ( fs.existsSync(path) ) {
      console.log("Found: " + path);
      let stats = fs.statSync(path);
      if ( !stats.isSocket() ) {
        continue;
      }
      // Try to connect if it's a new socket
      if ( connections.hasOwnProperty(path) ) {
        continue;
      }
      console.log(path + " is a new socket");
      var client = new net.Socket();
      client.nickname = path;
      client.connect(path, function() {
        let msg = "Connected at: " + client.nickname;
        console.log(msg);
        let request = { "command": ["request_log_messages", "info"], "request_id": requests["req_request_log_messages"] }
        client.write(JSON.stringify(request) + '\n');
        request = { "command": ["show-text", msg, 3000], "request_id": requests["req_show-text"] }
        client.write(JSON.stringify(request) + '\n');
      });
      connections[path] = new mpvConnection(path, client);
      console.log("New connection added: " + path);

      client.on('data', function(data) {
        console.log("Received: " + typeof(data) + " from " + client.nickname);
        console.log("Received    = " + data.toString());

        // MPV can send multiple messages on consecutive lines
        let responses = data.toString().split(/\r?\n/);

        for (let j = 0; j < responses.length; j++ ) {
          if ( responses[j].length > 0 ) {
            let jsonResponse = JSON.parse(responses[j]);

            if ( jsonResponse.hasOwnProperty("request_id") ) {
              // These are notifications we asked for with a request_id
              console.log("RI = " + jsonResponse.request_id);
              processRequestResponse(client.nickname, jsonResponse);
            } else
            if ( jsonResponse.hasOwnProperty("id") ) {
              // These are "builtin" mpv notifications (no request_id, just id)
              console.log("ID = " + jsonResponse.id);
              processEvent(client.nickname, jsonResponse);
            } else
            if ( jsonResponse.hasOwnProperty("event") && 
                  jsonResponse.event === "seek" ) {
              console.log("event = seek");
              seekMaster.initiate(client.nickname);
              seekMaster.dump();
              processEvent(client.nickname, jsonResponse);
            } else
            if ( jsonResponse.hasOwnProperty("event") && 
                  jsonResponse.event === "pause" ) {
              processEvent(client.nickname, jsonResponse);
            } else
            if ( jsonResponse.hasOwnProperty("event") && 
                  jsonResponse.event === "unpause" ) {
              processEvent(client.nickname, jsonResponse);
            }
            // any other msg types ??
          }
        }
      });

      client.on('close', function() {
        console.log('Connection closed by ' + client.nickname);
        delete connections[client.nickname];
      });
    }
  }


};

/*
  These events (with a request_id) are the immediate success/failure/whatever
  responses to specific requests which we have made (and nominated a request_id)
*/
function processRequestResponse(from, response) {
  console.log("Processing data from: " + from + ". Data: " + JSON.stringify(response));

  console.log("request_id = " + response.request_id);

  switch (response.request_id) {
    case requests["req_show-text"]: // Display "connected at ..." message
      console.log("displayed OK");
      // Since this is first interaction, now try to observe speed
      var request = { "command": ["observe_property", requests["req_observe-speed"]+100, "speed"], "request_id": requests["req_observe-speed"] }
      connections[from].write(JSON.stringify(request) + '\n');

    case requests["req_observe-speed"]: // Request to observe speed changes
      console.log("speed changes requested OK");
      break;

    case requests["req_get-time-pos"]:
      console.log("new time-pos " + response.data);
      //seekMaster.addTime(response.data);
      /*
        Don't broadcast this time-pos if it's from  slave,
        just add slave to seekMaster object
      */
      if ( seekMaster.isSlave(from) ) {
        seekMaster.update(from)
      } else {
        /*
          As master, send the new time-pos to all slaves
        */
        // First apply any desired offset
        var offsetTime = applyOffset(from, response.data);
        console.log("applying time offset: " + response.data + " is now: " + offsetTime);
        //request = { "command": ["set_property", "playback-time", response.data], "request_id": requests["req_get-time-pos"]+100 };
        request = { "command": ["set_property", "playback-time", offsetTime], "request_id": requests["req_get-time-pos"]+100 };
        broadcast(from, request);
      }
      break;

    default:
      break
  }
};

/*
  These events may be either
    - "builtin" with no id at all
    - events we have requested earlier and we nominated a particular id for.
    - events we have requested earlier without a nominated id (has id = 0).
*/
function processEvent(from, response) {
  console.log("Processing prop-change data from: " + from + ". Data: " + JSON.stringify(response));

  let request = {};
  if( response.hasOwnProperty("id") ) {
    console.log("id = " + response.id);
    switch (response.id) {
      case requests["req_observe-speed"]+100:
        // Should be a speed report but better check
        if ( response.name == "speed" ) {
          console.log("Speed report: " + response.data);
          if ( response.data != properties.speed ) {
            // broadcast new speed
            request = { "command": ["set_property", "speed", response.data] };
            broadcast(from, request);

            properties.speed = response.data;
          }
        }
        break;
      default:
        break;
    }
  } else {
    // something without any id
    if( response.hasOwnProperty("event") ) {
      switch ( response.event ) {
        case "seek":
          if ( seekMaster.isSlave(from) ) {
            seekMaster.update(from);
            break;
          }
          if ( ! seekMaster.hasTime() ) {
            console.log("seek event (" + from + ") - need to find time-pos");
            request = { "command": ["get_property", "time-pos"], "request_id": requests["req_get-time-pos"] };
            console.log("Sending request: " + JSON.stringify(request));
            connections[from].write(JSON.stringify(request) + '\n');
          } else {
            console.log("alreaday have a time-pos");
          }
          break;

        case "pause":
          request = { "command": ["set_property", "pause", true] };
          broadcast(from, request);
          break;

        case "unpause":
          request = { "command": ["set_property", "pause", false] };
          broadcast(from, request);
          break;

        default:
          break;
      }
    }
  }

};


/*
  Send data to all connections
  If "from" is a valid connection, don't send to that connection.
*/

function broadcast(from, request) {
  console.log("broadcasting ...");
  const entries = Object.entries(connections);
  for (const [name, connection] of entries) {
    if ( name === from ) {
      console.log("Don't broadcast to " + name);
      continue;
    }
    console.log("broadcasting to " + name + ": " + JSON.stringify(request));
    connection.write(JSON.stringify(request) + "\n");
  }

};

function applyOffset(from, intime) {
  console.log("applyOffset() from: " + from);

  var newtime = intime;
  var path = pathBase + "0";
  console.log("Primary is " + path);
  if ( from == path ) {
    newtime = intime - timeOffset;
  } else {
    newtime = intime + timeOffset;
  }

  if ( newtime < 0.0 ) {
    newtime = 0.0;
  }
  return newtime;
};

setInterval(makeNewConnections, 1500);



/* ex:set ai shiftwidth=2 inputtab=spaces smarttab noautotab: */

