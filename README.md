# mpv-sync

**mpv-sync** is a small javascript application which distributes a range of
events and property changes between multiple instances of the
[MPV video player](https://mpv.io/), the overall goal being to synchronise their
playback streams.

Communication between **mpv-sync** and the mpv instances uses IPC sockets
(1 per mpv instance) created by the *multisocket.lua* script (see below) as each
mpv instance starts up. At the moment, this means that all mpv instances must be
running on the same host machine as **mpv-sync** itself.


### Usage
`./reflect [offset]`
where *offset* is some amount of time (in seconds) by which to offset the first
mpv instance from the others.


### Requirements

1. **mpv** instances must have been built with support for javascript.
2. the host system must have [nodejs](https://nodejs.org/en/download/) installed
3. the user's *scripts* directory should contain this [multisocket.lua](https://raw.githubusercontent.com/AN3223/dotfiles/master/.config/mpv/scripts/multisocket.lua) script.

The user's *scripts* directory (which may need to be created) is most likely to be:
- Linux: $HOME/.config/mpv/scripts
- Windows:
- OSX:
